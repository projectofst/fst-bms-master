#####################################################################
#   FST BMS --- master
#
#   Makefile
#   ______________________________________________________________
#
#   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; version 2 of the
#   License only.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#####################################################################


# TODO
# 1.
# Add compilation rules for libraries of course.
# 2.
# Use compiler dependencies generation instead of makedepend: optimal output and versatility


CC := /opt/microchip/xc16/v1.21/bin/xc16-gcc
MCU := 30F6012A

PROGNAME := master

# Libs: -pthread: threads; m: math; ­GL ­GLU ­glut SDL: OpenGL
LIBS :=

# Directories
LIBDIR :=
SRCDIR := src/
BINDIR := bin/
OBJDIR := obj/

# extra include directories; add here
INCDIRS = ./


SOURCES  := $(wildcard $(SRCDIR)*.c) $(wildcard $(SRCDIR)*/*.c)
INCDIRS  += $(sort $(dir $(wildcard $(SRCDIR)*.h) $(wildcard $(SRCDIR)*/*.h)))
OBJECTS  := $(addprefix $(OBJDIR), $(SOURCES:.c=.o))

# Debug: -gx  x=0,1,(2),3; add here extra flags for conditional compilation like -DDBG defining DBG
DBG = -g0
# Optimization level: -Ox  x=(0),1,s,2,3,fast (see documentation for all the options)
OPT = -O0

# Generic gcc flags
CFLAGS = -Wall -Wextra $(DBG) $(OPT) $(foreach includedir,$(INCDIRS),-I$(includedir)) -c
LFLAGS = -Wall -Wextra $(DBG) $(OPT) $(foreach librarydir,$(LIBDIR),-L($librarydir)) $(LIBS)
# Specific job flags
CFLAGS += -mcpu=$(MCU) -omf=elf -msmart-io=1 -msfr-warn=off
LFLAGS += -mcpu=$(MCU) -omf=elf -Wl,--script=p30F6012A.gld,--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="$(BINDIR)BMSslave.map",--report-mem


.PHONY: all depend clean


#####################################################################
# Maintenance

all:
	@ make -j 16 $(BINDIR)$(PROGNAME).hex

depend:
	@ makedepend -Y -p$(OBJDIR) -- $(CFLAGS) $(LFLAGS) -- $(SOURCES)

clean:
	@ rm -f $(OBJECTS) $(BINDIR)*


#####################################################################
# Compilation rules

$(BINDIR)$(PROGNAME).hex: $(BINDIR)$(PROGNAME).elf
	/opt/microchip/xc16/v1.21/bin/xc16-bin2hex $(BINDIR)$(PROGNAME).elf

$(BINDIR)$(PROGNAME).elf: $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LFLAGS)


$(OBJDIR)$(SRCDIR)%.o: $(SRCDIR)%.c
	$(CC) -o $@ $< $(CFLAGS)


# makedepend after this line
# DO NOT DELETE THIS LINE

obj/src/master.o: Common/config.h Common/CANcodes.h src/config.h
obj/src/master.o: CAN_IDs/CAN_ID.h src/master.h src/monitoring/stack.h
obj/src/master.o: src/aux/CAN.h src/aux/timer.h src/control/io.h
obj/src/monitoring/stack.o: Common/config.h Common/CANcodes.h src/config.h
obj/src/monitoring/stack.o: CAN_IDs/CAN_ID.h src/master.h
obj/src/monitoring/stack.o: src/monitoring/stack.h src/control/charging.h
obj/src/monitoring/stack.o: src/aux/timer.h src/aux/CAN.h src/control/io.h
obj/src/control/io.o: src/master.h src/config.h CAN_IDs/CAN_ID.h
obj/src/control/io.o: src/monitoring/stack.h src/aux/delay.h src/control/io.h
obj/src/control/charging.o: src/master.h src/config.h CAN_IDs/CAN_ID.h
obj/src/control/charging.o: src/monitoring/stack.h
obj/src/aux/CAN.o: Common/config.h Common/CANcodes.h src/config.h
obj/src/aux/CAN.o: CAN_IDs/CAN_ID.h src/master.h src/monitoring/stack.h
obj/src/aux/CAN.o: src/control/io.h src/aux/CAN.h src/aux/delay.h
obj/src/aux/LED.o: src/config.h CAN_IDs/CAN_ID.h src/aux/delay.h
obj/src/aux/LED.o: src/aux/LED.h
obj/src/aux/timer.o: src/config.h CAN_IDs/CAN_ID.h src/master.h
obj/src/aux/timer.o: src/monitoring/stack.h src/aux/timer.h
