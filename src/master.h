/**********************************************************************
 *   FST BMS --- master
 *
 *   Main master module header
 *      - global parameters
 *      - global variables
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __MASTER_H__
#define __MASTER_H__

#include "config.h"
#include "stack.h"

/*
 * Global Variables; remember to initialize (!)
 */

#ifdef OWNER
	#define EXTERN
#else
	#define EXTERN extern	/* declares external variables */
#endif

/* careful with race conditions! */

EXTERN volatile unsigned long long _time;	/* time in ms since turn on (64bits) */

EXTERN volatile unsigned int _verbose;		/* flag for determining if extensive data should be reported to CAN2 */

EXTERN volatile unsigned int _total_SOC;	/* all percentages %*10 */
EXTERN volatile unsigned int _highest_SOC;
EXTERN volatile unsigned int _lowest_SOC;

EXTERN volatile unsigned int _total_V;		/* mV*0.1 */
EXTERN volatile unsigned int _highest_V;	/* mv*10 */
EXTERN volatile unsigned int _average_V;	/* mv*10 */
EXTERN volatile unsigned int _lowest_V;		/* mv*10 */

EXTERN volatile int _average_cell_T;		/* all temperatures ºC*10 */
EXTERN volatile int _highest_cell_T;
EXTERN volatile int _lowest_cell_T;
EXTERN volatile int _highest_uP_T;
EXTERN volatile int _lowest_uP_T;
EXTERN volatile int _highest_balancing_load_T;
EXTERN volatile int _lowest_balancing_load_T;


/* holds the threshold for balancing in mV*10; should start as V_CRITICAL_H and lowered by the master module */
EXTERN volatile unsigned int _balancing_target;

EXTERN volatile unsigned int _override;
EXTERN volatile unsigned int _monitor_period;	/* monitoring period in ms */
EXTERN volatile unsigned int _op_mode;			/* mode of operation */
EXTERN volatile int _AIR_on;					/* 1: on; 0: off */
EXTERN volatile int _AMS_error;		/* 1: error raised; 0: error clear */
EXTERN volatile unsigned int _status_flags_mask;		/* status flags mask */


EXTERN volatile stack _stacks[SLAVES_N];

/*
 * Prototypes
 */

int set_CPU_priority(unsigned int n);
int set_mode(unsigned int mode);
void main_loop(void);

#define RESET() {\
	__asm__ volatile("reset");\
}


#endif
