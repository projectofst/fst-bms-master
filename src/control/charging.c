/**********************************************************************
 *   FST BMS --- master
 *
 *   Charging
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>

#include "master.h"


/**********************************************************************
 * Name:	balance_target
 * Args:	-
 * Return:	unsigned int
 * Desc:	Returns the target for balancing according to balancing
 *          strategy.
 **********************************************************************/
unsigned int balance_target(void){

	/* relay the heuristics on the slaves: they know better their own temperature and limits */
	return _lowest_V;
}
