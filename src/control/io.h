/**********************************************************************
 *   FST BMS --- master
 *
 *   Master IO functions
 *   ______________________________________________________________
 *
 *   Copyright 2012 Miguel Silva <miguel.l.silva@tecnico.ulisboa.pt>
 *   Copyright 2013 Daniel Pinho <daniel.s.pinho@tecnico.ulisboa.pt>
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __IO_H__
#define __IO_H__


/* AMS error LED */
#define AMS_ERROR			LATDbits.LATD6
#define AIR_FEED			(!PORTDbits.RD10)


void IO_config(void);
void turn_off_sequence(void);
void turn_on_sequence(void);
void AMS_set_fault(void);
void AMS_clear_fault(void);

#endif
