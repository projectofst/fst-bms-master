/**********************************************************************
 *   FST BMS --- master
 *
 *   Master IO functions
 *   ______________________________________________________________
 *
 *   Copyright 2012 Miguel Silva <miguel.l.silva@tecnico.ulisboa.pt>
 *   Copyright 2013 Daniel Pinho <daniel.s.pinho@tecnico.ulisboa.pt>
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>
#include <libpic30.h>

#include "master.h"
#include "stack.h"
#include "delay.h"
#include "io.h"


/* AIR+ control */
#define P_AIR				LATDbits.LATD1
#define P_AIR_ON()			P_AIR = 1;
#define P_AIR_OFF()			P_AIR = 0;

/* AIR- control */
#define N_AIR				LATDbits.LATD2
#define N_AIR_ON()			N_AIR = 1;
#define N_AIR_OFF()			N_AIR = 0;

/* Pre-charge relay control */
#define P_CHARGE			LATDbits.LATD3
#define P_CHARGE_ON()		P_CHARGE = 1;
#define P_CHARGE_OFF()		P_CHARGE = 0;

/* Discharge relay control */
#define D_CHARGE			LATDbits.LATD4
#define D_CHARGE_ON()		D_CHARGE = 0;
#define D_CHARGE_OFF()		D_CHARGE = 1;


/* Setting AMS error cuts power to relays.              *
 * Abrupt change may cause damage to relays, making     *
 * it dangerous to do it without the turn off sequence. *
 * Therefore this macro shouldn't be accessible in its  *
 * raw form.                                            *
 * Anyway, there are more routines to do, so these      *
 * should never be called directly.                     */
#define AMS_ERROR_ON()		AMS_ERROR = 1;
#define AMS_ERROR_OFF()		AMS_ERROR = 0;


unsigned int _EEDATA(4) _eep_AMS_error;		/* 1: error raised; 0: error clear */
_prog_addressT _eep_AMS_error_addr;


/**********************************************************************
 * Name:	IO_config
 * Args:	-
 * Return:	-
 * Desc:	Configures IO ports and interruptions.
 **********************************************************************/
void IO_config(void){

	TRISDbits.TRISD1  = 0;	/* AIR+ */
	TRISDbits.TRISD2  = 0;	/* AIR- */
	TRISDbits.TRISD3  = 0;	/* Pre-Charge */
	TRISDbits.TRISD4  = 0;	/* Discharge */
	TRISDbits.TRISD6  = 0;	/* AMS Fault */
	TRISDbits.TRISD8  = 1;	/* Ignition */
	TRISDbits.TRISD11 = 1;	/* Reset Error */
	TRISDbits.TRISD10 = 1;	/* AIR Detection */


	/* Ignition configuration */
	INTCON2bits.INT1EP = 1;	/* INT1 (RD8) on the falling edge */
	IC1CONbits.ICSIDL = 0;	/* Enable in Idle mode */
	IFS1bits.INT1IF = 0;	/* Reset INT1 interruption flag */
	IPC4bits.INT1IP = 6;	/* Interrupt priority */
	IEC1bits.INT1IE = 1;	/* Enable INT1 Interrupt Service Routine */

	/* AIR power down detection */
	INTCON2bits.INT3EP = 0;	/* INT3 (RD10) on the rising edge */
	IC3CONbits.ICSIDL = 0;	/* Enable in Idle mode */
	IFS2bits.INT3IF = 0;	/* reset interruption flag */
	IPC9bits.INT3IP = 6;	/* interruption priority */
	IEC2bits.INT3IE = 1;	/* enable interruption */

	/* reset button configuration */
	INTCON2bits.INT4EP = 1;	/* INT4 (RD11) on the falling edge */
	IC4CONbits.ICSIDL = 0;	/* Enable in Idle mode */
	IFS2bits.INT4IF = 0;	/* reset interruption flag */
	IPC9bits.INT4IP = 6;	/* interruption priority */
	IEC2bits.INT4IE = 1;	/* enable interruption */

	/* initialize a variable to represent the Data EEPROM address */
	_init_prog_address(_eep_AMS_error_addr, _eep_AMS_error);

	/* read from EEPROM to local variable */
	_memcpy_p2d16((void *)&_AMS_error, _eep_AMS_error_addr, _EE_WORD);

	AMS_ERROR = _AMS_error;

	return;
}


/**********************************************************************
 * Name:	turn_off_sequence
 * Args:	-
 * Return:	-
 * Desc:	Opens AIRs and closes the discharge relay.
 **********************************************************************/
void turn_off_sequence(void){

	unsigned int prevIPL;

	/* critical zone: pilot could run the turn on sequence in the middle of this; *
	 * cannot have an interruption here;                                          */
	prevIPL = set_CPU_priority(6);

	P_AIR_OFF();
	__delay_ms(50);
	N_AIR_OFF();
	P_CHARGE_OFF();
	__delay_ms(50);
	D_CHARGE_ON();
	_AIR_on = 0;

	set_CPU_priority(prevIPL);

	return;
}


/**********************************************************************
 * Name:	turn_on_sequence
 * Args:	-
 * Return:	-
 * Desc:	Opens the discharge circuit and closes the AIRs with a
 *          pre-charge routine.
 **********************************************************************/
void turn_on_sequence(void){

	unsigned int prevIPL;
	int i;

	/* critical zone: not only time dependent but also CAN1 could run the turn *
	 * off sequence in the middle of this; cannot have a CAN interruption here */
	prevIPL = set_CPU_priority(6);

	/* Disable watchdog timer for ~5sec here. */
	RCONbits.SWDTEN = 0;

	if(!AMS_ERROR || _override){

		D_CHARGE_OFF();
		__delay_ms(50);
		N_AIR_ON();
		__delay_ms(50);
		P_CHARGE_ON();

		/* pre-charging */
		for(i=0;i<10;i++){
			__delay_ms(500);
			if(!AIR_FEED){		/* AIR power down */
				turn_off_sequence();
				return;
			}
		}

		P_AIR_ON();
		__delay_ms(50);
		P_CHARGE_OFF();
		_AIR_on = 1;
	}

	/* Enable watchdog timer again. */
	RCONbits.SWDTEN = 1;

	set_CPU_priority(prevIPL);

	return;
}


/**********************************************************************
 * Name:	AMS_set_fault
 * Args:	-
 * Return:	-
 * Desc:	Sets AMS fault state.
 **********************************************************************/
void AMS_set_fault(void){

	turn_off_sequence();
	AMS_ERROR_ON();
	_AMS_error = 1;

	/* Erase a word in Data EEPROM at _eep_AMS_error_addr */
	_erase_eedata(_eep_AMS_error_addr, _EE_WORD);
	_wait_eedata();

	/* Write a word (_AMS_error) to Data EEPROM */
	_write_eedata_word(_eep_AMS_error_addr, _AMS_error);
	_wait_eedata();

	return;
}


/**********************************************************************
 * Name:	AMS_clear_fault
 * Args:	-
 * Return:	-
 * Desc:	Sets AMS fault state.
 **********************************************************************/
void AMS_clear_fault(void){


	AMS_ERROR_OFF();
	_AMS_error = 0;

	/* Erase a word in Data EEPROM at _eep_AMS_error_addr */
	_erase_eedata(_eep_AMS_error_addr, _EE_WORD);
	_wait_eedata();

	/* Write a word (_AMS_error) to Data EEPROM */
	_write_eedata_word(_eep_AMS_error_addr, _AMS_error);
	_wait_eedata();

	return;
}


/**********************************************************************
 * Assign INT1 interruption
 **********************************************************************/
void __attribute__((interrupt, no_auto_psv)) _INT1Interrupt(void){

	if(_AIR_on == 0){
		turn_on_sequence();
	}
	else{
		turn_off_sequence();
	}

	IFS1bits.INT1IF = 0;	/* Clear the INT1 interruption flag or else */
	return;
}


/**********************************************************************
 * Assign INT3 interruption
 **********************************************************************/
void __attribute__((interrupt, no_auto_psv)) _INT3Interrupt(void){

	turn_off_sequence();

	IFS2bits.INT3IF = 0;
	return;
}


/**********************************************************************
 * Assign INT4 interruption
 **********************************************************************/
void __attribute__((interrupt, no_auto_psv)) _INT4Interrupt(void){

	AMS_clear_fault();

	IFS2bits.INT4IF = 0;
	return;
}
