/**********************************************************************
 *   FST BMS --- master
 *
 *   LED
 *      - pin configuration
 *      - functions to control LED
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>

#include "config.h"
#include "delay.h"
#include "LED.h"


/**********************************************************************
 * Name:	LED_config
 * Args:	-
 * Return:	-
 * Desc:	Configures LED port (RD11) to be used as digital output.
 **********************************************************************/
/*void LED_config(void){
	TRISDbits.TRISD11 = 0;	* LED port as output *
	return;
}*/


/**********************************************************************
 * Name:	LED_blink
 * Args:	int n, unsigned int Ton, unsigned int Toff
 * Return:	-
 * Desc:	Blinks LED n according to Ton and Toff in ms
 **********************************************************************/
/*void LED_blink(int n, unsigned int Ton, unsigned int Toff){
	int i;
	for(i=0;i<n;i++){
		LED_on();
		__delay_ms(Ton/2);
		LED_off();
		__delay_ms(Toff/2);
	}
	return;
}*/

/**********************************************************************
 * Name:	LED_on
 * Args:	-
 * Return:	exit status
 * Desc:	Turns LED on. Returns -1 if LED already on.
 **********************************************************************/
/*int LED_on(void){
	if(LATDbits.LATD11 == 0){
		LATDbits.LATD11 = 1;
		return 0;
	}
	return -1;
}*/

/**********************************************************************
 * Name:	LED_off
 * Args:	-
 * Return:	exit status
 * Desc:	Turns LED off. Returns -1 if LED already off.
 **********************************************************************/
/*int LED_off(void){
	if(LATDbits.LATD11 == 1){
		LATDbits.LATD11 = 0;
		return 0;
	}
	return -1;
}*/
