/**********************************************************************
 *   FST BMS --- master
 *
 *   Timer
 *      - module configuration
 *      - interruption assignment
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>

#include "config.h"
#include "master.h"
#include "timer.h"


/**********************************************************************
 * Name:	timer1_config
 * Args:	-
 * Return:	-
 * Desc:	Configures and enables timer 1 module.
 **********************************************************************/
void timer1_config(void){

	T1CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T1CONbits.TGATE = 0;		/* Gated mode off                        */
	T1CONbits.TCKPS = 0;		/* prescale 1:1                          */
	T1CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR1 = 0;					/* clears the timer register             */
	PR1 = M_SEC_1_1;			/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC0bits.T1IP = 7;			/* Timer 1 Interrupt Priority 0-7        */
	IFS0bits.T1IF = 0;			/* clear interrupt flag                  */
	IEC0bits.T1IE = 1;			/* Timer 1 Interrupt Enable              */


	T1CONbits.TON = 1;			/* starts the timer                      */
	return;
}


/**********************************************************************
 * Assign Timer 1 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T1Interrupt (void){

	_time++;

	IFS0bits.T1IF = 0;		/* clears interruption flag */
	return;
}


/**********************************************************************
 * Name:	timer2_config
 * Args:	-
 * Return:	-
 * Desc:	Configures and enables timer 2 module.
 **********************************************************************/
void timer2_config(void){

	T2CONbits.TCS   = 0;				/* use internal clock: Fcy               */
	T2CONbits.TGATE = 0;				/* Gated mode off                        */
	T2CONbits.TCKPS = 3;				/* prescale 1:256                        */
	T2CONbits.TSIDL = 0;				/* don't stop the timer in idle          */

	TMR2 = 0;							/* clears the timer register             */

	/*PR2 = M_SEC_1_256*MT_NORMAL;*/	/* value at which the register overflows *
										 * and raises T2IF; set in               *
										 * update_loop_period()                  */

	/* interruptions */
	IPC1bits.T2IP = 2;					/* Timer 3 Interrupt Priority 0-7        */
	IFS0bits.T2IF = 0;					/* clear interrupt flag                  */
	IEC0bits.T2IE = 1;					/* Timer 3 Interrupt Enable              */


	T2CONbits.TON = 1;					/* starts the timer                      */
	return;
}


/**********************************************************************
 * Assign Timer 2 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T2Interrupt (void){

	main_loop();

	IFS0bits.T2IF = 0;		/* clears interruption flag */
	return;
}


/**********************************************************************
 * Name:	update_loop_period
 * Args:	unsigned int period
 * Return:	-
 * Desc:	Configures timer responsible for main loop for 'period' in
 *          milliseconds and updates global parameter.
 **********************************************************************/
void update_loop_period(unsigned int period){

	/* WARNING: (M_SEC_1_256*period) != (period*M_SEC_1_256):          *
	 * M_SEC_1_256 = 30000000UL/4/256*0.001  which means               *
	 * (period*M_SEC_1_256) =ex.= 1000 * 30000000UL /4/256*0.001       *
	 * and 1000*30000000UL overflows!                                  */

	PR2 = (M_SEC_1_256 * period);
	_monitor_period = period;

	return;
}
