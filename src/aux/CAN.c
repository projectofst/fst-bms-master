/**********************************************************************
 *   FST BMS --- master
 *
 *   CAN
 *      - device configuration
 *      - interruption assignment
 *      - top level communication functions
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>

#include "Common/config.h"
#include "Common/CANcodes.h"
#include "config.h"
#include "master.h"
#include "io.h"
#include "CAN.h"
#include "delay.h"

/* prototypes of 'local' functions */
int CAN1_send(CANdata *CANmessage);
int CAN2_send(CANdata *CANmessage);


/**********************************************************************
 * Name:	CAN1_config
 * Args:	-
 * Return:	-
 * Desc:	Configures CAN 1 channel.
 **********************************************************************/
void CAN1_config(void){

	/* assuming FOSC 30MHz */

	TRISFbits.TRISF0 = 1;			/* CANRX input */
	TRISFbits.TRISF1 = 0;			/* CANTX output */

	C1CTRLbits.REQOP = 4;			/* request configuration mode */
	while(C1CTRLbits.OPMODE!=4);	/* wait for mode to be set */

	/* control */
	C1CTRLbits.CANCAP = 0;			/* disable CAN capture */
	C1CTRLbits.CSIDL = 0;			/* continue on Idle mode */
	C1CTRLbits.CANCKS = 0;			/* master clock 4*FCY */

	/* baudrate */
	C1CFG1bits.BRP = 0;				/* TQ = 2/FCAN */
	C1CFG2bits.SEG2PHTS = 1;		/* Freely programmable */
	C1CFG2bits.PRSEG = 1;			/* propagation time segment bits length 2 x TQ */
	C1CFG1bits.SJW = 0;				/* re-synchronization jump width time is 1 x TQ */
	C1CFG2bits.SEG1PH = 7;			/* phase 1 segment length 8 x TQ */
	C1CFG2bits.SEG2PH = 4;			/* phase 2 segment length 5 x TQ <= phase1+prop.seg. && > resync jump */

	/* receive buffers */
	C1RX0CONbits.DBEN = 1;			/* RX0 full will write to RX1 */
	C1RXF0SID = 8;					/* probably both wrong (filter set but not used) and unnecessary ???!!!! */
	C1RXF1SID = 8;					/* " */
	C1RXM0SID = 0;					/* match all messages */
	C1RXM1SID = 0;					/* " */

	/* interrupts */
	C1INTEbits.RX0IE = 1;			/* RX0 interrupt enabled */
	C1INTEbits.RX1IE = 1;			/* RX1 interrupt enabled */
	IFS1bits.C1IF = 0;				/* Clear CAN1 Flag */
	IPC6bits.C1IP = 5;				/* CAN1 priority is 5 */
	IEC1bits.C1IE = 1;				/* CAN1 interrupts enabled */

	C1TX0CONbits.TXPRI = 3;
	C1RX0CONbits.RXFUL = 0;
	C1RX1CONbits.RXFUL = 0;

	C1CTRLbits.REQOP = 0;			/* request normal mode */
	while(C1CTRLbits.OPMODE!=0);	/* wait for normal mode to be set */

	return;
}


/**********************************************************************
 * Name:	CAN1_send
 * Args:	CANdata *CANmessage
 * Return:	exit status
 * Desc:	Sends CAN message.
 **********************************************************************/
int CAN1_send(CANdata *CANmessage){

	static int try_n=0;
	unsigned int int_sid=0;

	int_sid = CANmessage->sid<<5;
	int_sid = int_sid & (0b1111100000000000);
	int_sid = int_sid | ((CANmessage->sid<<2)&(0b0000000011111100));

	if(!C1TX0CONbits.TXREQ){	/* occupied? */
		C1TX0SID = int_sid;
		C1TX0SIDbits.TXIDE = 0;
		C1TX0SIDbits.SRR = 0;
		C1TX0DLC = 0;
		C1TX0DLCbits.DLC = CANmessage->dlc;
		C1TX0B1 = CANmessage->data[0];
		C1TX0B2 = CANmessage->data[1];
		C1TX0B3 = CANmessage->data[2];
		C1TX0B4 = CANmessage->data[3];
		C1TX0CONbits.TXREQ = 1;
	}else if(!C1TX1CONbits.TXREQ){
		C1TX1SID = int_sid;
		C1TX1SIDbits.TXIDE = 0;
		C1TX1SIDbits.SRR = 0;
		C1TX1DLC = 0;
		C1TX1DLCbits.DLC = CANmessage->dlc;
		C1TX1B1 = CANmessage->data[0];
		C1TX1B2 = CANmessage->data[1];
		C1TX1B3 = CANmessage->data[2];
		C1TX1B4 = CANmessage->data[3];
		C1TX1CONbits.TXREQ = 1;
	}else if(!C1TX2CONbits.TXREQ){
		C1TX2SID = int_sid;
		C1TX2SIDbits.TXIDE = 0;
		C1TX2SIDbits.SRR = 0;
		C1TX2DLC = 0;
		C1TX2DLCbits.DLC = CANmessage->dlc;
		C1TX2B1 = CANmessage->data[0];
		C1TX2B2 = CANmessage->data[1];
		C1TX2B3 = CANmessage->data[2];
		C1TX2B4 = CANmessage->data[3];
		C1TX2CONbits.TXREQ = 1;

	}else if(try_n > 2){
		/* too many failures, clear buffers and return error */
		try_n = 0;
		C1TX0CONbits.TXREQ = 0;
		C1TX1CONbits.TXREQ = 0;
		C1TX2CONbits.TXREQ = 0;
		return -2;
	}else{
		try_n++;
		return -1;
	}
	/* reset number of tries */

	try_n = 0;
	__delay_us(200);
	return 0;
}


/**********************************************************************
 * Name:	CAN2_config
 * Args:	-
 * Return:	-
 * Desc:	Configures CAN 2 channel.
 **********************************************************************/
void CAN2_config(void){

	/* assuming FOSC 30MHz */

	TRISGbits.TRISG0 = 1;			/* CANRX input */
	TRISGbits.TRISG1 = 0;			/* CANTX output */

	C2CTRLbits.REQOP = 4;			/* request configuration mode */
	while(C2CTRLbits.OPMODE!=4);	/* wait for mode to be set */

	/* control */
	C2CTRLbits.CANCAP = 0;			/* disable CAN capture */
	C2CTRLbits.CSIDL = 0;			/* continue on Idle mode */
	C2CTRLbits.CANCKS = 0;			/* master clock 4*FCY */

	/* baudrate */
	C2CFG1bits.BRP = 0;				/* TQ = 2/FCAN */
	C2CFG2bits.SEG2PHTS = 1;		/* Freely programmable */
	C2CFG2bits.PRSEG = 1;			/* propagation time segment bits length 2 x TQ */
	C2CFG1bits.SJW = 0;				/* re-synchronization jump width time is 1 x TQ */
	C2CFG2bits.SEG1PH = 7;			/* phase 1 segment length 8 x TQ */
	C2CFG2bits.SEG2PH = 4;			/* phase 2 segment length 5 x TQ <= phase1+prop.seg. && > resync jump */

	/* receive buffers */
	C2RX0CONbits.DBEN = 1;			/* RX0 full will write to RX1 */
	C2RXF0SID = 8;					/* probably both wrong (filter set but not used) and unnecessary ???!!!! */
	C2RXF1SID = 8;					/* " */
	C2RXM0SID = 0;					/* match all messages */
	C2RXM1SID = 0;					/* " */

	/* interrupts */
	C2INTEbits.RX0IE = 1;			/* RX0 interrupt enabled */
	C2INTEbits.RX1IE = 1;			/* RX1 interrupt enabled */
	IFS2bits.C2IF = 0;				/* Clear CAN2 Flag */
	IPC9bits.C2IP = 5;				/* CAN2 priority is 5 */
	IEC2bits.C2IE = 1;				/* CAN2 interrupts enabled */

	C2TX0CONbits.TXPRI = 3;
	C2RX0CONbits.RXFUL = 0;
	C2RX1CONbits.RXFUL = 0;

	C2CTRLbits.REQOP = 0;			/* request normal mode */
	while(C2CTRLbits.OPMODE!=0);	/* wait for normal mode to be set */

	return;
}


/**********************************************************************
 * Name:	CAN2_send
 * Args:	CANdata *CANmessage
 * Return:	exit status
 * Desc:	Sends CAN message.
 **********************************************************************/
int CAN2_send(CANdata *CANmessage){

	static int try_n=0;
	unsigned int int_sid=0;

	int_sid = CANmessage->sid<<5;
	int_sid = int_sid & (0b1111100000000000);
	int_sid = int_sid | ((CANmessage->sid<<2)&(0b0000000011111100));

	if(!C2TX0CONbits.TXREQ){	/* occupied? */
		C2TX0SID = int_sid;
		C2TX0SIDbits.TXIDE = 0;
		C2TX0SIDbits.SRR = 0;
		C2TX0DLC = 0;
		C2TX0DLCbits.DLC = CANmessage->dlc;
		C2TX0B1 = CANmessage->data[0];
		C2TX0B2 = CANmessage->data[1];
		C2TX0B3 = CANmessage->data[2];
		C2TX0B4 = CANmessage->data[3];
		C2TX0CONbits.TXREQ = 1;
	}else if(!C2TX1CONbits.TXREQ){
		C2TX1SID = int_sid;
		C2TX1SIDbits.TXIDE = 0;
		C2TX1SIDbits.SRR = 0;
		C2TX1DLC = 0;
		C2TX1DLCbits.DLC = CANmessage->dlc;
		C2TX1B1 = CANmessage->data[0];
		C2TX1B2 = CANmessage->data[1];
		C2TX1B3 = CANmessage->data[2];
		C2TX1B4 = CANmessage->data[3];
		C2TX1CONbits.TXREQ = 1;
	}else if(!C2TX2CONbits.TXREQ){
		C2TX2SID = int_sid;
		C2TX2SIDbits.TXIDE = 0;
		C2TX2SIDbits.SRR = 0;
		C2TX2DLC = 0;
		C2TX2DLCbits.DLC = CANmessage->dlc;
		C2TX2B1 = CANmessage->data[0];
		C2TX2B2 = CANmessage->data[1];
		C2TX2B3 = CANmessage->data[2];
		C2TX2B4 = CANmessage->data[3];
		C2TX2CONbits.TXREQ = 1;

	}else if(try_n > 2){
		/* too many failures, clear buffers and return error */
		try_n = 0;
		C2TX0CONbits.TXREQ = 0;
		C2TX1CONbits.TXREQ = 0;
		C2TX2CONbits.TXREQ = 0;
		return -2;
	}else{
		try_n++;
		return -1;
	}
	/* reset number of tries */
	try_n = 0;
	__delay_us(200);
	return 0;
}


/**********************************************************************
 * Name:	CAN1_handler
 * Args:	CANdata msg
 * Return:	exit status
 * Desc:	Parses received message for correctness and executes
 *          appropriate functions.
 **********************************************************************/
int CAN1_handler(CANdata msg){

	/* do nothing; discard */
	if(_op_mode == OP_INITIALIZING){
		return 0;
	}


	if((msg.sid >= CAN_ID_BMS_SLAVE(0)) && (msg.sid <= CAN_ID_BMS_SLAVE(11))){


		switch(msg.data[1]){

			/*
			 * Queries' answers
			 */

			case CAN_Q_START_PING:
				break;
			case CAN_Q_PING:
				break;

			case CAN_Q_SOC:
				store_SOC(msg.sid - CAN_ID_BMS_SLAVE(0), msg.data[2]);
				break;

			case CAN_Q_TB_SOC:
				store_tb_SOC(msg.sid - CAN_ID_BMS_SLAVE(0), msg.data[2], msg.data[3]);
				break;

			case CAN_Q_TB_VOLT:
				store_tb_voltages(msg.sid - CAN_ID_BMS_SLAVE(0), msg.data[2], msg.data[3]);
				break;
			case CAN_Q_AVG_VOLT:
				store_avg_voltages(msg.sid - CAN_ID_BMS_SLAVE(0), msg.data[2]);
				break;

			/*case CAN_Q_SOH:
				break;*/

			case CAN_Q_TB_CTEMP:
				store_cell_tb_temperature(msg.sid - CAN_ID_BMS_SLAVE(0), msg.data[2], msg.data[3]);
				break;
			case CAN_Q_AVG_CTEMP:
				store_cell_avg_temperature(msg.sid - CAN_ID_BMS_SLAVE(0), msg.data[2]);
				break;

			case CAN_Q_STEMP:
				store_slave_temperatures(msg.sid - CAN_ID_BMS_SLAVE(0), msg.data[2], msg.data[3]);
				break;

			case CAN_Q_BALANCING:
				store_balancing_mask(msg.sid - CAN_ID_BMS_SLAVE(0), msg.data[2]);
				break;

			case CAN_Q_FMASKS:
				store_fault_masks(msg.sid - CAN_ID_BMS_SLAVE(0), msg.data[2], msg.data[3]);
				break;

			case CAN_Q_VOLTAGES_0:
			case CAN_Q_VOLTAGES_1:
			case CAN_Q_VOLTAGES_2:
			case CAN_Q_VOLTAGES_3:
			case CAN_Q_VOLTAGES_4:
			case CAN_Q_VOLTAGES_5:
				store_voltages(msg.sid - CAN_ID_BMS_SLAVE(0), msg.data[1]-CAN_Q_VOLTAGES_0, msg.data[2], msg.data[3]);
				break;

			case CAN_Q_CTEMPS_0:
			case CAN_Q_CTEMPS_1:
			case CAN_Q_CTEMPS_2:
			/*case CAN_Q_CTEMPS_3:
			case CAN_Q_CTEMPS_4:
			case CAN_Q_CTEMPS_5:*/
				store_cell_temperatures(msg.sid - CAN_ID_BMS_SLAVE(0), msg.data[1]-CAN_Q_CTEMPS_0, msg.data[2], msg.data[3]);
				break;

			/*case CAN_Q_SELF_CHECK:
				break;*/

			/*case CAN_Q_DEMAND_RESET:
				break;*/


			/*
			 * Emergencies
			 * TODO!
			 * Ignoring for now; to mirror to CAN2 it has to be outside of the CAN1 interruption to avoid lock up!
			 */

			/* Critical state */
			case CAN_E_V_CRITICAL_L:
			case CAN_E_V_CRITICAL_H:

			case CAN_E_SLAVE_T_CRITICAL_H:
			case CAN_E_SLAVE_T_CRITICAL_L:

			case CAN_E_CELL_T_CRITICAL_H:
			case CAN_E_CELL_T_CRITICAL_L:

			case CAN_E_S_V_CONNECTIONS:
			case CAN_E_S_T_CONNECTIONS:


			/* Alert state */
			case CAN_E_V_ALERT_H:
			case CAN_E_V_ALERT_L:
			case CAN_E_CELL_T_ALERT_H:
			case CAN_E_CELL_T_ALERT_L:
			case CAN_E_SLAVE_T_ALERT_H:
			case CAN_E_SLAVE_T_ALERT_L:

			case CAN_E_S_GENERIC_FAULT:
				break;


			/* Code does not exist or is CAN_E_UNKNOWN itself.. either way default to: */
			default:
				return -1;
		}

	}
	/* else: not for this module; ignore */

	return 0;
}


/**********************************************************************
 * Name:	CAN2_handler
 * Args:	CANdata msg
 * Return:	exit status
 * Desc:	Parses received message for correctness and executes
 *          appropriate functions.
 **********************************************************************/
int CAN2_handler(CANdata msg){

	CANdata reply;

	/* do nothing; discard */
	if(_op_mode == OP_INITIALIZING){
		return 0;
	}

	/* if CAN_ID_BMS_CONTROL_* */
	if(((msg.sid >= 350) || (msg.sid <= 359)) && (msg.dlc == 4)){
		switch(msg.data[1]){

			/*case CAN_Q_SELF_CHECK:
				break;*/

			case CAN_Q_DEMAND_RESET:
				msg.sid = CAN_ID_BMS_MASTER;
				while(CAN2_send(&msg)==-1);
				__delay_ms(50);	/* wait (at least) for message to be sent */
				RESET();
				break;

			case CAN_Q_TOGGLE_TS:
				msg.sid = CAN_ID_BMS_MASTER;
				if(_AIR_on){
					turn_off_sequence();
				}else{
					turn_on_sequence();
				}
				while(CAN2_send(&msg)==-1);
				break;

			case CAN_Q_RESET_ERROR:
				msg.sid = CAN_ID_BMS_MASTER;
				AMS_clear_fault();
				while(CAN2_send(&msg)==-1);
				break;

			case CAN_Q_VERBOSE:
				msg.sid = CAN_ID_BMS_MASTER;
				_verbose = (_verbose + 1)&1;	/* toggle functionality; 0 or 1 */
				while(CAN2_send(&msg)==-1);
				break;

			case CAN_Q_FAKE_ERROR:
				AMS_set_fault();
				msg.sid = CAN_ID_BMS_MASTER;
				while(CAN2_send(&msg)==-1);
				break;

			case CAN_Q_OVERRIDE:
				AMS_clear_fault();
				_override = 1;
				msg.sid = CAN_ID_BMS_MASTER;
				while(CAN2_send(&msg)==-1);
				break;

			default:
				reply.sid = CAN_ID_BMS_MASTER;
				reply.dlc = 4;
				reply.data[0] = _time;
				reply.data[1] = CAN_Q_UNKNOWN;
				while(CAN2_send(&reply)==-1);
				break;
		}

	}else if(((msg.sid >= 350) || (msg.sid <= 359)) && (msg.dlc == 6)){
		switch(msg.data[1]){

			case CAN_B_SET_MODE:
				set_mode(msg.data[2]);
				break;

			default:
				reply.sid = CAN_ID_BMS_MASTER;
				reply.dlc = 4;
				reply.data[0] = _time;
				reply.data[1] = CAN_Q_UNKNOWN;
				while(CAN2_send(&reply)==-1);
				break;
		}

	}else{
		return -1;
	}

	return 0;
}


/**********************************************************************
 * Assign CAN1 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv)) _C1Interrupt(void){

	CANdata msg;

	if(C1INTFbits.RX0IF){
		msg.sid = C1RX0SIDbits.SID;
		msg.dlc = C1RX0DLCbits.DLC;
		msg.data[0] = C1RX0B1;
		msg.data[1] = C1RX0B2;
		msg.data[2] = C1RX0B3;
		msg.data[3] = C1RX0B4;
		C1RX0CONbits.RXFUL = 0;
		C1INTFbits.RX0IF = 0;

		CAN1_handler(msg);
	}
	if(C1INTFbits.RX1IF){
		msg.sid = C1RX1SIDbits.SID;
		msg.dlc = C1RX1DLCbits.DLC;
		msg.data[0] = C1RX1B1;
		msg.data[1] = C1RX1B2;
		msg.data[2] = C1RX1B3;
		msg.data[3] = C1RX1B4;
		C1RX1CONbits.RXFUL = 0;
		C1INTFbits.RX1IF = 0;

		CAN1_handler(msg);
	}

	IFS1bits.C1IF=0;
	return;
}


/**********************************************************************
 * Assign CAN2 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv)) _C2Interrupt(void){

	CANdata msg;

	if(C2INTFbits.RX0IF){
		msg.sid = C2RX0SIDbits.SID;
		msg.dlc = C2RX0DLCbits.DLC;
		msg.data[0] = C2RX0B1;
		msg.data[1] = C2RX0B2;
		msg.data[2] = C2RX0B3;
		msg.data[3] = C2RX0B4;
		C2RX0CONbits.RXFUL = 0;
		C2INTFbits.RX0IF = 0;

		CAN2_handler(msg);
	}
	if(C2INTFbits.RX1IF){
		msg.sid = C2RX1SIDbits.SID;
		msg.dlc = C2RX1DLCbits.DLC;
		msg.data[0] = C2RX1B1;
		msg.data[1] = C2RX1B2;
		msg.data[2] = C2RX1B3;
		msg.data[3] = C2RX1B4;
		C2RX1CONbits.RXFUL = 0;
		C2INTFbits.RX1IF = 0;

		CAN2_handler(msg);
	}

	IFS2bits.C2IF=0;
	return;
}


/**********************************************************************
 * Name:	query
 * Args:	unsigned int id, unsigned int code
 * Return:	-
 * Desc:	Sends query to specified slave module id (to CAN 1).
 **********************************************************************/
void query(unsigned int id, unsigned int code){

	CANdata query;

	switch(code){

		case CAN_Q_PING:
		case CAN_Q_SOC:
		case CAN_Q_TB_SOC:
		case CAN_Q_SOH:
		case CAN_Q_TB_VOLT:
		case CAN_Q_AVG_VOLT:
		case CAN_Q_TB_CTEMP:
		case CAN_Q_AVG_CTEMP:
		case CAN_Q_STEMP:
		case CAN_Q_BALANCING:
		case CAN_Q_FMASKS:

		case CAN_Q_VOLTAGES_0:
		case CAN_Q_VOLTAGES_1:
		case CAN_Q_VOLTAGES_2:
		case CAN_Q_VOLTAGES_3:
		case CAN_Q_VOLTAGES_4:
		case CAN_Q_VOLTAGES_5:

		case CAN_Q_CTEMPS_0:
		case CAN_Q_CTEMPS_1:
		case CAN_Q_CTEMPS_2:
		case CAN_Q_CTEMPS_3:
		case CAN_Q_CTEMPS_4:
		case CAN_Q_CTEMPS_5:

		case CAN_Q_SELF_CHECK:

		case CAN_Q_DEMAND_RESET:

			query.sid = CAN_ID_BMS_QUERY;
			query.dlc = 6;
			query.data[0] = _time;
			query.data[1] = code;
			query.data[2] = CAN_ID_BMS_SLAVE(id);

			/* guarantees the message is sent, even if at the time all the outgoing buffers are full */
			while(CAN1_send(&query)==-1);

			break;

		/* Code does not exist; won't be processed by any slave... so don't send it */
		default:
			break;
	}

	return;
}


/**********************************************************************
 * Name:	broadcast
 * Args:	unsigned int id, unsigned int code
 * Return:	-
 * Desc:	Sends broadcast to specified slave module id (to CAN 1).
 **********************************************************************/
void broadcast(unsigned int code){

	CANdata broadcast;

	broadcast.sid = CAN_ID_BMS_BROADCAST;
	broadcast.data[0] = _time;
	broadcast.data[1] = code;

	switch(code){

		case CAN_B_BALANCING_TARGET:
			broadcast.dlc = 6;
			broadcast.data[2] = _balancing_target;
			break;

		case CAN_B_SET_MODE:
			broadcast.dlc = 6;
			broadcast.data[2] = _op_mode;
			break;

		/* Code does not exist; won't be processed by any slave... so don't send it */
		default:
			return;
	}

	/* guarantees the message is sent, even if at the time all the outgoing buffers are full */
	while(CAN1_send(&broadcast)==-1);

	return;
}


/**********************************************************************
 * Name:	emit_emergency
 * Args:	unsigned int code
 * Return:	-
 * Desc:	Sends respective CAN emergency message (to CAN 2).
 **********************************************************************/
void emit_emergency(unsigned int module_n, unsigned int code){

	CANdata AlertMsg;

	AlertMsg.sid = CAN_ID_BMS_FAULT;

	/* emergency independent part of message */
	AlertMsg.dlc = 6;
	AlertMsg.data[0] = _time;
	AlertMsg.data[1] = module_n;

	switch(code){

		/* critical and alert pairs have the same message apart from the code which is set above *
		 * So... no need to break, as it results in a smaller and simpler code                   */
		case CAN_E_V_CRITICAL_H:
		case CAN_E_V_ALERT_H:

		case CAN_E_V_CRITICAL_L:
		case CAN_E_V_ALERT_L:

		case CAN_E_CELL_T_CRITICAL_H:
		case CAN_E_CELL_T_ALERT_H:

		case CAN_E_CELL_T_CRITICAL_L:
		case CAN_E_CELL_T_ALERT_L:

		case CAN_E_SLAVE_T_CRITICAL_H:
		case CAN_E_SLAVE_T_ALERT_H:

		case CAN_E_SLAVE_T_CRITICAL_L:
		case CAN_E_SLAVE_T_ALERT_L:

		case CAN_E_S_V_CONNECTIONS:

		case CAN_E_S_T_CONNECTIONS:
		case CAN_E_S_GENERIC_FAULT:

		/* master specific */
		case CAN_E_BMS_DOS_SOC:
		case CAN_E_BMS_DOS_SOH:
		case CAN_E_BMS_DOS_VOLT:
		case CAN_E_BMS_DOS_TEMP:
		case CAN_E_BMS_DOS_MASKS:

		case CAN_E_BMS_GENERIC_FAULT:

			AlertMsg.data[2] = code;
			break;

		/* Code does not exist or is CAN_E_UNKNOWN itself.. either way default to: */
		default:
			AlertMsg.data[2] = CAN_E_UNKNOWN;
			break;
	}

	/* guarantees the message is sent, even if at the time all the outgoing buffers are full */
	while(CAN2_send(&AlertMsg)==-1);

	return;
}


/**********************************************************************
 * Name:	broadcast_status
 * Args:	unsigned int verbose
 * Return:	-
 * Desc:	Broadcasts global battery status (to CAN 2).
 **********************************************************************/
void broadcast_status(unsigned int verbose){

	CANdata msg;

	msg.data[0] = _time;

	msg.sid = CAN_ID_BMS_SOC;
	msg.dlc = 8;
	msg.data[1] = _total_SOC;
	msg.data[2] = _highest_SOC;
	msg.data[3] = _lowest_SOC;
	while(CAN2_send(&msg)==-1);

/*msg.sid = CAN_ID_BMS_SOH;*/

	msg.sid = CAN_ID_BMS_VOLT;
	msg.dlc = 4;
	msg.data[1] = _total_V;
	while(CAN2_send(&msg)==-1);

	msg.sid = CAN_ID_BMS_CELL_VOLT;
	msg.dlc = 8;
	msg.data[1] = _highest_V;
	msg.data[2] = _average_V;
	msg.data[3] = _lowest_V;
	while(CAN2_send(&msg)==-1);


	msg.sid = CAN_ID_BMS_CELL_TEMP;
	msg.dlc = 8;
	msg.data[1] = _highest_cell_T;
	msg.data[2] = _average_cell_T;
	msg.data[3] = _lowest_cell_T;
	while(CAN2_send(&msg)==-1);

	msg.sid = CAN_ID_BMS_SLAVE_TEMP;
	msg.dlc = 6;
	msg.data[1] = _highest_uP_T;
	msg.data[2] = _highest_balancing_load_T;
	while(CAN2_send(&msg)==-1);


/*msg.sid = CAN_ID_BMS_CURRENT;*/


	msg.sid = CAN_ID_BMS_OP_MODE;
	msg.dlc = 6;
	msg.data[1] = _op_mode;
	msg.data[2] = _status_flags_mask;
	while(CAN2_send(&msg)==-1);


	/*
	 * Verbose mode
	 * (the slave IDs can be used since it's a different line of CAN)
	 */

	if(verbose){

		/* for all slaves */
		int id, i;
		for(id=0;id<SLAVES_N;id++){

			/* total (average) SOC */
			msg.sid = CAN_ID_BMS_SLAVE(id);
			msg.dlc = 6;
			msg.data[1] = CAN_Q_SOC;
			msg.data[2] = _stacks[id].soc.SOC;
			while(CAN2_send(&msg)==-1);

			/* top and bottom SOC */
			msg.sid = CAN_ID_BMS_SLAVE(id);
			msg.dlc = 8;
			msg.data[1] = CAN_Q_TB_SOC;
			msg.data[2] = _stacks[id].soc.highest_SOC;
			msg.data[3] = _stacks[id].soc.lowest_SOC;
			while(CAN2_send(&msg)==-1);

			/* top and bottom voltage */
			msg.sid = CAN_ID_BMS_SLAVE(id);
			msg.dlc = 8;
			msg.data[1] = CAN_Q_TB_VOLT;
			msg.data[2] = _stacks[id].volt.highest;
			msg.data[3] = _stacks[id].volt.lowest;
			while(CAN2_send(&msg)==-1);

			/* average voltage */
			msg.sid = CAN_ID_BMS_SLAVE(id);
			msg.dlc = 6;
			msg.data[1] = CAN_Q_AVG_VOLT;
			msg.data[2] = _stacks[id].volt.avg;
			while(CAN2_send(&msg)==-1);

			/* voltages */
			for(i=0;i<SLAVES_N/2;i++){
				msg.sid = CAN_ID_BMS_SLAVE(id);
				msg.dlc = 8;
					msg.data[1] = i + CAN_Q_VOLTAGES_0;
				msg.data[2] = _stacks[id].volt.voltages[i*2];
				msg.data[3] = _stacks[id].volt.voltages[1+i*2];
				while(CAN2_send(&msg)==-1);
			}

			/* top and bottom cell temperatures */
			msg.sid = CAN_ID_BMS_SLAVE(id);
			msg.dlc = 8;
			msg.data[1] = CAN_Q_TB_CTEMP;
			msg.data[2] = _stacks[id].temp.highest_cell;
			msg.data[3] = _stacks[id].temp.lowest_cell;
			while(CAN2_send(&msg)==-1);

			/* average cell temperature */
			msg.sid = CAN_ID_BMS_SLAVE(id);
			msg.dlc = 6;
			msg.data[1] = CAN_Q_AVG_CTEMP;
			msg.data[2] = _stacks[id].temp.avg_cell;
			while(CAN2_send(&msg)==-1);

			/* cell temperatures */
			{
				int nmsgs = TEMP_SENSORS_N/2;
				if(TEMP_SENSORS_N%2 != 0){
					nmsgs++;
				}

				for(i=0;i<nmsgs;i++){
					msg.sid = CAN_ID_BMS_SLAVE(id);
					msg.dlc = 8;
					msg.data[1] = i + CAN_Q_CTEMPS_0;
					/*if(i*2 < TEMP_SENSORS_N){*/
						msg.data[2] = _stacks[id].temp.cell_temperatures[i*2];
					/*}else{
						msg.data[2] = 0;
					}
					if(1+i*2 < TEMP_SENSORS_N){*/
						msg.data[3] = _stacks[id].temp.cell_temperatures[1+i*2];
					/*}else{
						msg.data[3] = 0;
					}*/
					while(CAN2_send(&msg)==-1);
				}
			}

			/* slave temperatures */
			msg.sid = CAN_ID_BMS_SLAVE(id);
			msg.dlc = 8;
			msg.data[1] = CAN_Q_STEMP;
			msg.data[2] = _stacks[id].temp.uP;
			msg.data[3] = _stacks[id].temp.balancing_load;
			while(CAN2_send(&msg)==-1);

			/* fault masks */
			msg.sid = CAN_ID_BMS_SLAVE(id);
			msg.dlc = 8;
			msg.data[1] = CAN_Q_FMASKS;
			msg.data[2] = _stacks[id].mask.broken_V_connections;
			msg.data[3] = _stacks[id].mask.broken_T_connections;
			while(CAN2_send(&msg)==-1);

			/* balancing mask */
			msg.sid = CAN_ID_BMS_SLAVE(id);
			msg.dlc = 6;
			msg.data[1] = CAN_Q_BALANCING;
			msg.data[2] = _stacks[id].mask.balancing;
			while(CAN2_send(&msg)==-1);

		}
	}


	return;
}
