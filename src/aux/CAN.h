/**********************************************************************
 *   FST BMS --- master
 *
 *   CAN
 *      - data structures
 *      - functions prototypes
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __CAN_H__
#define __CAN_H__

/*
 * Data structures
 */

typedef struct CANDATA {
	unsigned int sid;
	unsigned int dlc;
	unsigned int data[4];
} CANdata;


/*
 * Prototypes
 */

void CAN1_config(void);
void CAN2_config(void);

/* CAN 1 */
void query(unsigned int id, unsigned int code);
void broadcast(unsigned int code);
void emit_emergency(unsigned int module_ID, unsigned int code);

/* CAN 2 */
void broadcast_status(unsigned int verbose);

#endif
