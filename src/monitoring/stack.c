/**********************************************************************
 *   FST BMS --- master
 *
 *   Stack monitoring
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>
#include <limits.h>

#include "Common/config.h"
#include "Common/CANcodes.h"
#include "config.h"
#include "master.h"
#include "charging.h"
#include "timer.h"
#include "CAN.h"
#include "io.h"


/**********************************************************************
 * Name:	stack_init
 * Args:	-
 * Return:	-
 * Desc:	Initializes the global vector of stacks (slaves).
 **********************************************************************/
void stack_init(void){

	unsigned int i, j;

	for(i=0;i<(SLAVES_N);i++){

		/* should recover values from permanent memory */
		/* if these values are used to test shutdown conditions, they may need to be initialized differently or else there will be emergencies being broadcasted at startup */

		/* SOC */

		_stacks[i].soc.SOC = 0;
		_stacks[i].soc.highest_SOC = 0;
		_stacks[i].soc.lowest_SOC = 0;
		_stacks[i].soc.SOC_unanswered_requests = 0;


		/* Voltages */

		for(j=0;j<12;j++){
			_stacks[i].volt.voltages[j] = 0;
			_stacks[i].volt.Volt_unanswered_requests[(j+1)/2] = 0;
		}

		_stacks[i].volt.highest = 0;
		_stacks[i].volt.lowest = 0;
		_stacks[i].volt.Volt_tb_unanswered_requests = 0;

		_stacks[i].volt.avg = 0;
		_stacks[i].volt.Volt_avg_unanswered_requests = 0;

		_stacks[i].volt.high_faults = 0;
		_stacks[i].volt.low_faults = 0;


		/* Temperatures */

		for(j=0;j<12;j++){
			_stacks[i].temp.cell_temperatures[j] = 0;
			_stacks[i].temp.Cell_temp_unanswered_requests[(j+1)/2] = 0;
		}

		_stacks[i].temp.highest_cell = 0;
		_stacks[i].temp.lowest_cell = 0;
		_stacks[i].temp.Cell_temp_tb_unanswered_requests = 0;

		_stacks[i].temp.avg_cell = 0;
		_stacks[i].temp.Cell_temp_avg_unanswered_requests = 0;

		_stacks[i].temp.high_cell_faults = 0;
		_stacks[i].temp.low_cell_faults = 0;

		_stacks[i].temp.uP = 0;
		_stacks[i].temp.balancing_load = 0;
		_stacks[i].temp.Slave_temp_unanswered_requests = 0;

		_stacks[i].temp.high_slave_faults = 0;
		_stacks[i].temp.low_slave_faults = 0;


		/* Masks */

		_stacks[i].mask.balancing = 0;
		_stacks[i].mask.Balancing_unanswered_requests = 0;

		_stacks[i].mask.broken_V_connections = 0;
		_stacks[i].mask.broken_T_connections = 0;
		_stacks[i].mask.Fault_masks_unanswered_requests = 0;

		_stacks[i].mask.broken_V_connections_faults = 0;
		_stacks[i].mask.broken_T_connections_faults = 0;

	}

	return;
}


/**********************************************************************
 * Name:	monitor_stacks
 * Args:	-
 * Return:	-
 * Desc:	Checks for outdated values and requests an update.
 **********************************************************************/
void monitor_stacks(void){

	unsigned int i;
	static unsigned int id = 0;


	/*
	 * Query values
	 */


	/* SOC */

	if(_stacks[id].soc.SOC_unanswered_requests >= DOS_ALLOWED_FAULTS){
		emit_emergency(id, CAN_E_BMS_DOS_SOC);
		AMS_set_fault();
	}
	if(_stacks[id].soc.SOC_unanswered_requests < DOS_ALLOWED_FAULTS){
		_stacks[id].soc.SOC_unanswered_requests ++;
	}
	query(id, CAN_Q_SOC);

	if(_stacks[id].soc.SOC_tb_unanswered_requests >= DOS_ALLOWED_FAULTS){
		emit_emergency(id, CAN_E_BMS_DOS_SOC);
		AMS_set_fault();
	}
	if(_stacks[id].soc.SOC_tb_unanswered_requests < DOS_ALLOWED_FAULTS){
		_stacks[id].soc.SOC_tb_unanswered_requests ++;
	}
	query(id, CAN_Q_TB_SOC);


	/* Voltages */

	if(_stacks[id].volt.Volt_tb_unanswered_requests >= DOS_ALLOWED_FAULTS){
		emit_emergency(id, CAN_E_BMS_DOS_VOLT);
		AMS_set_fault();
	}
	if(_stacks[id].volt.Volt_tb_unanswered_requests < DOS_ALLOWED_FAULTS){
		_stacks[id].volt.Volt_tb_unanswered_requests ++;
	}
	query(id, CAN_Q_TB_VOLT);

	if(_stacks[id].volt.Volt_avg_unanswered_requests >= DOS_ALLOWED_FAULTS){
		emit_emergency(id, CAN_E_BMS_DOS_VOLT);
		AMS_set_fault();
	}
	if(_stacks[id].volt.Volt_avg_unanswered_requests < DOS_ALLOWED_FAULTS){
		_stacks[id].volt.Volt_avg_unanswered_requests ++;
	}
	query(id, CAN_Q_AVG_VOLT);

	for(i=0;i<6;i++){
		if(_stacks[id].volt.Volt_unanswered_requests[i] >= DOS_ALLOWED_FAULTS){
			emit_emergency(id, CAN_E_BMS_DOS_VOLT);
			AMS_set_fault();
		}
		if(_stacks[id].volt.Volt_unanswered_requests[i] < DOS_ALLOWED_FAULTS){
			_stacks[id].volt.Volt_unanswered_requests[i] ++;
		}
		query(id, CAN_Q_VOLTAGES(i));
	}


	/* Temperatures */

	if(_stacks[id].temp.Cell_temp_tb_unanswered_requests >= DOS_ALLOWED_FAULTS){
		emit_emergency(id, CAN_E_BMS_DOS_TEMP);
		AMS_set_fault();
	}
	if(_stacks[id].temp.Cell_temp_tb_unanswered_requests < DOS_ALLOWED_FAULTS){
		_stacks[id].temp.Cell_temp_tb_unanswered_requests ++;
	}
	query(id, CAN_Q_TB_CTEMP);

	if(_stacks[id].temp.Cell_temp_avg_unanswered_requests >= DOS_ALLOWED_FAULTS){
		emit_emergency(id, CAN_E_BMS_DOS_TEMP);
		AMS_set_fault();
	}
	if(_stacks[id].temp.Cell_temp_avg_unanswered_requests < DOS_ALLOWED_FAULTS){
		_stacks[id].temp.Cell_temp_avg_unanswered_requests ++;
	}
	query(id, CAN_Q_AVG_CTEMP);

	for(i=0;i<(TEMP_SENSORS_N/2+TEMP_SENSORS_N%2);i++){
		if(_stacks[id].temp.Cell_temp_unanswered_requests[i] >= DOS_ALLOWED_FAULTS){
			emit_emergency(id, CAN_E_BMS_DOS_TEMP);
			AMS_set_fault();
		}
		if(_stacks[id].temp.Cell_temp_unanswered_requests[i] < DOS_ALLOWED_FAULTS){
			_stacks[id].temp.Cell_temp_unanswered_requests[i] ++;
		}
		query(id, CAN_Q_CTEMPS(i));
	}

	if(_stacks[id].temp.Slave_temp_unanswered_requests >= DOS_ALLOWED_FAULTS){
		emit_emergency(id, CAN_E_BMS_DOS_TEMP);
		AMS_set_fault();
	}
	if(_stacks[id].temp.Slave_temp_unanswered_requests < DOS_ALLOWED_FAULTS){
		_stacks[id].temp.Slave_temp_unanswered_requests ++;
	}
	query(id, CAN_Q_STEMP);


	/* Masks */

	if(_stacks[id].mask.Balancing_unanswered_requests >= DOS_ALLOWED_FAULTS){
		emit_emergency(id, CAN_E_BMS_DOS_MASKS);
		AMS_set_fault();
	}
	if(_stacks[id].mask.Balancing_unanswered_requests < DOS_ALLOWED_FAULTS){
		_stacks[id].mask.Balancing_unanswered_requests ++;
	}
	query(id, CAN_Q_BALANCING);

	if(_stacks[id].mask.Fault_masks_unanswered_requests >= DOS_ALLOWED_FAULTS){
		emit_emergency(id, CAN_E_BMS_DOS_MASKS);
		AMS_set_fault();
	}
	if(_stacks[id].mask.Fault_masks_unanswered_requests < DOS_ALLOWED_FAULTS){
		_stacks[id].mask.Fault_masks_unanswered_requests ++;
	}
	query(id, CAN_Q_FMASKS);


	/*
	 * Check for emergency conditions
	 * There may be updates since new queries were issued, but the most recent are the ones that matter anyway
	 */

	/* if override is set and number of faults overflows it gets to 0 again; not a (serious) issue though... */

	/* High V */
	if(_stacks[id].volt.highest > V_CRITICAL_H){
		emit_emergency(id, CAN_E_V_CRITICAL_H);
		if((_stacks[id].volt.high_faults > CRITICAL_VALUES_ALLOWED_FAULTS) && !_override){

			/* TODO: If battery is completely full, this could be triggered while discharging, not making much sense.          *
			 * Some other check/logic should be implemented to protect in case battery is being charged without charging mode. */
			if(_op_mode == OP_CHARGING){
				AMS_set_fault();
			}

		}else{
			_stacks[id].volt.high_faults ++;
		}
	}else{
		_stacks[id].volt.high_faults = 0;
	}

	/* Low V */
	if(_stacks[id].volt.lowest < V_CRITICAL_L){
		emit_emergency(id, CAN_E_V_CRITICAL_L);
		if((_stacks[id].volt.low_faults > CRITICAL_VALUES_ALLOWED_FAULTS) && !_override){
			AMS_set_fault();
		}else{
			_stacks[id].volt.low_faults ++;
		}
	}else{
		_stacks[id].volt.low_faults = 0;
	}

	/* High cell T */
	if(((_op_mode != OP_CHARGING) && (_stacks[id].temp.highest_cell > TCD_CRITICAL_H)) || ((_op_mode == OP_CHARGING) && (_stacks[id].temp.highest_cell > TCC_CRITICAL_H))){
		emit_emergency(id, CAN_E_CELL_T_CRITICAL_H);
		if((_stacks[id].temp.high_cell_faults > CRITICAL_VALUES_ALLOWED_FAULTS) && !_override){
			AMS_set_fault();
		}else{
			_stacks[id].temp.high_cell_faults ++;
		}
	}else{
		_stacks[id].temp.high_cell_faults = 0;
	}

	/* Low cell T */
	if(((_op_mode != OP_CHARGING) && (_stacks[id].temp.lowest_cell < TCD_CRITICAL_L)) || ((_op_mode == OP_CHARGING) && (_stacks[id].temp.lowest_cell < TCC_CRITICAL_L))){
		emit_emergency(id, CAN_E_CELL_T_CRITICAL_H);
		if((_stacks[id].temp.low_cell_faults > CRITICAL_VALUES_ALLOWED_FAULTS) && !_override){
			AMS_set_fault();
		}else{
			_stacks[id].temp.low_cell_faults ++;
		}
	}else{
		_stacks[id].temp.low_cell_faults = 0;
	}

	/* High slave T */
	if(_stacks[id].temp.uP > TS_CRITICAL_H){
		emit_emergency(id, CAN_E_SLAVE_T_CRITICAL_H);
		if((_stacks[id].temp.high_slave_faults > CRITICAL_VALUES_ALLOWED_FAULTS) && !_override){
			AMS_set_fault();
		}else{
			_stacks[id].temp.high_slave_faults ++;
		}
	}else{
		_stacks[id].temp.high_slave_faults = 0;
	}

	/* Low slave T */
	if(_stacks[id].temp.uP < TS_CRITICAL_L){
		emit_emergency(id, CAN_E_SLAVE_T_CRITICAL_L);
		if((_stacks[id].temp.low_slave_faults > CRITICAL_VALUES_ALLOWED_FAULTS) && !_override){
			AMS_set_fault();
		}else{
			_stacks[id].temp.low_slave_faults ++;
		}
	}else{
		_stacks[id].temp.low_slave_faults = 0;
	}

	/* Bad V connection */
	if(_stacks[id].mask.broken_V_connections != 0){
		emit_emergency(id, CAN_E_S_V_CONNECTIONS);
		if((_stacks[id].mask.broken_V_connections_faults > CRITICAL_VALUES_ALLOWED_FAULTS) && !_override){
			AMS_set_fault();
		}else{
			_stacks[id].mask.broken_V_connections_faults ++;
		}
	}else{
		_stacks[id].mask.broken_V_connections_faults = 0;
	}

	/* Bad T connection */
	if(_stacks[id].mask.broken_T_connections != 0){
		emit_emergency(id, CAN_E_S_T_CONNECTIONS);
		if((_stacks[id].mask.broken_T_connections_faults > CRITICAL_VALUES_ALLOWED_FAULTS) && !_override){
			AMS_set_fault();
		}else{
			_stacks[id].mask.broken_T_connections_faults ++;
		}
	}else{
		_stacks[id].mask.broken_T_connections_faults = 0;
	}


	/* increment slave ID to be queried next */
	id = (id+1)%SLAVES_N;


	/*
	 * Data processing
	 * !!! to be made incrementally on store_* functions !!!
	 * There may be updates since new queries were issued, but the most recent are the ones that matter anyway
	 */

	{

		/* auxiliary variables for totals/averages calculations */
		unsigned aux_SOC = 0;
		unsigned long aux_V = 0;
		unsigned long aux_avgcellT = 0;

		/* consider protecting this code if these values are used elsewhere asynchronously */
		_highest_SOC  = 0;
		_lowest_SOC  = UINT_MAX;

		_highest_V = 0;
		_lowest_V  = UINT_MAX;

		_highest_cell_T = 0;
		_lowest_cell_T  = INT_MAX;

		_highest_uP_T = 0;
		_lowest_uP_T  = INT_MAX;

		_highest_balancing_load_T = 0;
		_lowest_balancing_load_T  = INT_MAX;


		for(i=0;i<SLAVES_N;i++){

			aux_SOC += _stacks[i].soc.SOC;
			aux_V += _stacks[i].volt.avg;

			if(_highest_SOC < _stacks[i].soc.highest_SOC){
				_highest_SOC = _stacks[i].soc.highest_SOC;
			}

			if(_lowest_SOC > _stacks[i].soc.lowest_SOC){
				_lowest_SOC = _stacks[i].soc.lowest_SOC;
			}

			if(_highest_V < _stacks[i].volt.highest){
				_highest_V = _stacks[i].volt.highest;
			}
			if(_lowest_V > _stacks[i].volt.lowest){
				_lowest_V = _stacks[i].volt.lowest;
			}

			if(_highest_cell_T < _stacks[i].temp.highest_cell){
				_highest_cell_T = _stacks[i].temp.highest_cell;
			}
			if(_lowest_cell_T > _stacks[i].temp.lowest_cell){
				_lowest_cell_T = _stacks[i].temp.lowest_cell;
			}

			if(_highest_uP_T < _stacks[i].temp.uP){
				_highest_uP_T = _stacks[i].temp.uP;
			}
			if(_lowest_uP_T > _stacks[i].temp.uP){
				_lowest_uP_T = _stacks[i].temp.uP;
			}

			if(_highest_balancing_load_T < _stacks[i].temp.balancing_load){
				_highest_balancing_load_T = _stacks[i].temp.balancing_load;
			}
			if(_lowest_balancing_load_T > _stacks[i].temp.balancing_load){
				_lowest_balancing_load_T = _stacks[i].temp.balancing_load;
			}

			aux_avgcellT += _stacks[i].temp.avg_cell;
		}

		_total_SOC = (unsigned int) (aux_SOC / SLAVES_N);		/* it's actually an average of percentages */
		_total_V = (unsigned int) (aux_V * CELLS_S_N / 100);	/* mV*0.1 */	/* sum_all_stacks/slaves(avgV) * CELLS_S_N / 100 for scale */
		_average_V = (unsigned int) (aux_V / SLAVES_N);			/* mV*10 */
		_average_cell_T = (unsigned int) (aux_avgcellT / SLAVES_N);

	}

	return;
}


/**********************************************************************
 * Name:	sync_slaves
 * Args:	-
 * Return:	-
 * Desc:	Synchronizes slaves' mode of operation and balancing
 *          targets.
 **********************************************************************/
void sync_slaves(void){

	/*
	 * Status
	 * Synchronization
	 */

	/* synchronize operating mode */
	broadcast(CAN_B_SET_MODE);

	if((_op_mode == OP_CHARGING) || (_op_mode == OP_REGENERATION)){
		_balancing_target = balance_target();
		broadcast(CAN_B_BALANCING_TARGET);
	}

	return;
}


/**********************************************************************
 * Name:	data_log
 * Args:	-
 * Return:	-
 * Desc:	Logs data (not implemented yet; no external memory) and
 *          issues additional queries according to relevant flags.
 **********************************************************************/
void data_log(void){

	return;
}


/**********************************************************************
 * Name:	store_SOC
 * Args:	unsigned int id, unsigned int SOC
 * Return:	-
 * Desc:	Stores values and resets number of unanswered requests.
 **********************************************************************/
void store_SOC(unsigned int id, unsigned int SOC){

	_stacks[id].soc.SOC = SOC;

	_stacks[id].soc.SOC_unanswered_requests = 0;

	return;
}


/**********************************************************************
 * Name:	store_tb_SOC
 * Args:	unsigned int id, unsigned int highest_SOC,
 *          unsigned int lowest_SOC
 * Return:	-
 * Desc:	Stores values and resets number of unanswered requests.
 **********************************************************************/
void store_tb_SOC(unsigned int id, unsigned int highest_SOC, unsigned int lowest_SOC){

	_stacks[id].soc.highest_SOC = highest_SOC;
	_stacks[id].soc.lowest_SOC = lowest_SOC;

	_stacks[id].soc.SOC_tb_unanswered_requests = 0;

	return;
}


/**********************************************************************
 * Name:	store_voltages
 * Args:	unsigned int id, unsigned int index, unsigned int volt1,
 *          unsigned int volt2
 * Return:	-
 * Desc:	Stores values and resets number of unanswered requests.
 **********************************************************************/
void store_voltages(unsigned int id, unsigned int index, unsigned int volt1, unsigned int volt2){

	_stacks[id].volt.voltages[index*2] = volt1;
	_stacks[id].volt.voltages[1+index*2] = volt2;

	_stacks[id].volt.Volt_unanswered_requests[index] = 0;

	return;
}

/**********************************************************************
 * Name:	store_tb_voltages
 * Args:	unsigned int id, unsigned int high_V, unsigned int low_V
 * Return:	-
 * Desc:	Stores values and resets number of unanswered requests.
 **********************************************************************/
void store_tb_voltages(unsigned int id, unsigned int high_V, unsigned int low_V){

	_stacks[id].volt.highest = high_V;
	_stacks[id].volt.lowest = low_V;

	_stacks[id].volt.Volt_tb_unanswered_requests = 0;

	return;
}


/**********************************************************************
 * Name:	store_avg_voltages
 * Args:	unsigned int id, unsigned int avg_V
 * Return:	-
 * Desc:	Stores values and resets number of unanswered requests.
 **********************************************************************/
void store_avg_voltages(unsigned int id, unsigned int avg_V){

	_stacks[id].volt.avg = avg_V;

	_stacks[id].volt.Volt_avg_unanswered_requests = 0;

	return;
}


/**********************************************************************
 * Name:	store_cell_tb_temperature
 * Args:	unsigned int id, unsigned int high_cell,
 *          unsigned int lowest_cell
 * Return:	-
 * Desc:	Stores values and resets number of unanswered requests.
 **********************************************************************/
void store_cell_tb_temperature(unsigned int id, unsigned int high_cell, unsigned int lowest_cell){

	_stacks[id].temp.highest_cell = high_cell;
	_stacks[id].temp.lowest_cell = lowest_cell;

	_stacks[id].temp.Cell_temp_tb_unanswered_requests = 0;

	return;
}


/**********************************************************************
 * Name:	store_cell_avg_temperature
 * Args:	unsigned int id, unsigned int avg_T
 * Return:	-
 * Desc:	Stores values and resets number of unanswered requests.
 **********************************************************************/
void store_cell_avg_temperature(unsigned int id, unsigned int avg_T){

	_stacks[id].temp.avg_cell = avg_T;

	_stacks[id].temp.Cell_temp_avg_unanswered_requests = 0;

	return;
}


/**********************************************************************
 * Name:	store_cell_temperatures
 * Args:	unsigned int id, unsigned int index, int temp1, int temp2
 * Return:	-
 * Desc:	Stores values and resets number of unanswered requests.
 **********************************************************************/
void store_cell_temperatures(unsigned int id, unsigned int index, int temp1, int temp2){

	_stacks[id].temp.cell_temperatures[index*2] = temp1;
	_stacks[id].temp.cell_temperatures[1+index*2] = temp2;

	_stacks[id].temp.Cell_temp_unanswered_requests[index] = 0;

	return;
}


/**********************************************************************
 * Name:	store_slave_temperatures
 * Args:	unsigned int id, int uP, int load
 * Return:	-
 * Desc:	Stores values and resets number of unanswered requests.
 **********************************************************************/
void store_slave_temperatures(unsigned int id, int uP, int load){

	_stacks[id].temp.uP = uP;
	_stacks[id].temp.balancing_load = load;

	_stacks[id].temp.Slave_temp_unanswered_requests = 0;

	return;
}


/**********************************************************************
 * Name:	store_balancing_mask
 * Args:	unsigned int id, unsigned int mask
 * Return:	-
 * Desc:	Stores values and resets number of unanswered requests.
 **********************************************************************/
void store_balancing_mask(unsigned int id, unsigned int mask){

	_stacks[id].mask.balancing = mask;

	_stacks[id].mask.Balancing_unanswered_requests = 0;

	return;
}

/**********************************************************************
 * Name:	store_fault_masks
 * Args:	unsigned int id, unsigned int bad_V_connection,
 *          unsigned int bad_T_connection
 * Return:	-
 * Desc:	Stores values and resets number of unanswered requests.
 **********************************************************************/
void store_fault_masks(unsigned int id, unsigned int bad_V_connection, unsigned int bad_T_connection){

	_stacks[id].mask.broken_V_connections = bad_V_connection;
	_stacks[id].mask.broken_T_connections = bad_T_connection;

	_stacks[id].mask.Fault_masks_unanswered_requests = 0;

	return;
}
