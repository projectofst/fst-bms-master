/**********************************************************************
 *   FST BMS --- master
 *
 *   Stack monitoring
 *      - data structures
 *      - functions prototypes
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

#ifndef __STACK_H__
#define __STACK_H__


/*
 * Data structures
 */

typedef struct SOC{

	unsigned int SOC;			/* %*10 */
	unsigned int highest_SOC;	/* %*10 */
	unsigned int lowest_SOC;	/* %*10 */
	unsigned int SOC_unanswered_requests;
	unsigned int SOC_tb_unanswered_requests;

}soc;


typedef struct VOLTAGES{

	unsigned int voltages[12];
	unsigned int Volt_unanswered_requests[6];

	unsigned int highest;
	unsigned int lowest;
	unsigned int Volt_tb_unanswered_requests;

	unsigned int avg;
	unsigned int Volt_avg_unanswered_requests;

	unsigned int high_faults;
	unsigned int low_faults;

}voltages;


typedef struct TEMPERATURES{

	int cell_temperatures[12];	/* 12 NTC, but only TEMP_SENSORS_N are used */
	unsigned int Cell_temp_unanswered_requests[6];

	int highest_cell;
	int lowest_cell;
	unsigned int Cell_temp_tb_unanswered_requests;

	int avg_cell;
	unsigned int Cell_temp_avg_unanswered_requests;

	unsigned int high_cell_faults;
	unsigned int low_cell_faults;

	int uP;
	int balancing_load;
	unsigned int Slave_temp_unanswered_requests;

	unsigned int high_slave_faults;
	unsigned int low_slave_faults;

}temperatures;


typedef struct MASKS{

	unsigned int balancing;
	unsigned int Balancing_unanswered_requests;

	unsigned int broken_V_connections;
	unsigned int broken_T_connections;
	unsigned int Fault_masks_unanswered_requests;

	unsigned int broken_V_connections_faults;
	unsigned int broken_T_connections_faults;

}masks;


typedef struct STACK{

	soc soc;
	voltages volt;
	temperatures temp;
	masks mask;

}stack;


/*
 * Prototypes
 */

void stack_init(void);

void monitor_stacks(void);
void sync_slaves(void);
void data_log(void);
void grace_period(unsigned int temp);

void store_SOC(unsigned int id, unsigned int SOC);
void store_tb_SOC(unsigned int id, unsigned int highest_SOC, unsigned int lowest_SOC);

void store_voltages(unsigned int id, unsigned int index, unsigned int volt1, unsigned int volt2);
void store_tb_voltages(unsigned int id, unsigned int high_V, unsigned int low_V);
void store_avg_voltages(unsigned int id, unsigned int avg_V);

void store_cell_tb_temperature(unsigned int id, unsigned int high_cell, unsigned int lowest_cell);
void store_cell_avg_temperature(unsigned int id, unsigned int avg_T);
void store_cell_temperatures(unsigned int id, unsigned int index, int temp1, int temp2);
void store_slave_temperatures(unsigned int id, int uP, int load);

void store_balancing_mask(unsigned int id, unsigned int mask);
void store_fault_masks(unsigned int id, unsigned int bad_V_connection, unsigned int bad_T_connection);

#endif
