/**********************************************************************
 *   FST BMS --- master
 *
 *   Main
 *      - device configuration
 *      - main process
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>

#include "Common/config.h"
#include "Common/CANcodes.h"
#include "config.h"

/* Only place where globals are not declared as 'extern' */
#define OWNER
#include "master.h"
#undef OWNER

#include "CAN.h"
#include "timer.h"
#include "stack.h"
#include "io.h"


/*
 * Configuration bits
 */

/*
Clock switching and Fail-safe clock disabled
External clock multiplier = 4/2 = 2
*/
_FOSC(CSW_FSCM_OFF & HS2_PLL4);

/*
Watch-dog timer off --- enabling it in software instead after initializing period
Prescalers make time out ~3,072 seconds (RC oscillator! may oscillate with supply voltage)
*/
_FWDT(WDT_OFF & WDTPSB_3 & WDTPSA_512);

/*
Brown Out voltage = 2.7V & BOR circuit disabled
Power On Reset timer = 16ms
/MCLR enabled
*/
_FBORPOR(BORV_27 & PBOR_OFF & PWRT_16 & MCLR_EN);

/*
Code protection off
*/
_FGS(CODE_PROT_OFF);

/*
Select comm channel
*/
_FICD(PGD);



/**********************************************************************
 * Name:	set_CPU_priority
 * Args:    unsigned int n
 * Return:	previous priority OR error code
 * Desc:	Sets CPU priority to n, provided it is between 0 and 7.
 *          Provides a way to disable interruptions in critical parts
 *          of the code.
 *          Needs to be set to 6 or lower to allow interruptions,
 *          depending on interruptions' priorities.
 *          Previous priority returned for restoring purposes.
 **********************************************************************/
int set_CPU_priority(unsigned int n){

	unsigned int prevIPL;

	if(n<=7){
		prevIPL = SRbits.IPL;
		SRbits.IPL = n; 			/* Status Register . CPU priority 0-7 */
		return prevIPL;
	}
	return -1;
}


/**********************************************************************
 * Name:	set_mode
 * Args:    unsigned int mode
 * Return:	exit status
 * Desc:	Sets operating mode of the master module.
 **********************************************************************/
int set_mode(unsigned int mode){

	switch(mode){

		/*case OP_INITIALIZING:
			return -1;*/

		case OP_NORMAL:
			_op_mode = mode;
			_balancing_target = V_CRITICAL_H;	/* so that when charging mode is set, no balancing is done until a new target is set */
			break;

		case OP_REGENERATION:
		case OP_CHARGING:
			_op_mode = mode;
			break;

		/* mode not recognized */
		default:
			return -1;
	}

	return 0;
}


/**********************************************************************
 * Name:	set_flags
 * Args:    -
 * Return:	-
 * Desc:	Updates a glabal mask with all the status flags.
 **********************************************************************/
void set_flags(void){

	_status_flags_mask = 0;

	_status_flags_mask |= (_AIR_on << MASK_TS_ON);
	_status_flags_mask |= (AMS_ERROR << MASK_AMS_ERROR);
	_status_flags_mask |= (AIR_FEED << MASK_AIR_FEED);

	return;
}


/**********************************************************************
 * Name:	main_loop
 * Args:    -
 * Return:	-
 * Desc:	Main loop.
 **********************************************************************/
void main_loop(void){

	static int timetoreport = 0;

	/* Assuming all is fine; gives a chance of reverting to normal after an alarm. *
	 * Should be the first thing in the loop: anything else should be able to set  *
	 * an alarm and increase the monitoring frequency                              */
	_monitor_period = MT_NORMAL;

	monitor_stacks();
	sync_slaves();

	set_flags();

	/*
	 * Broadcast status
	 * Once per round
	 */

	if(timetoreport == SLAVES_N){
		broadcast_status(_verbose);
		timetoreport = 0;
	}else{
		timetoreport++;
	}

	/*data_log();*/

	/* Clear watchdog timer so that device won't reset.     *
	 * If not run, device will reset in ~6sec (see config.) *
	 * Turn off sequence takes ~5sec!                       */
	ClrWdt();

	return;
}


int main(void){

	/*
	 * Globals init
	 */

	_time = 0;

	_verbose = 0;	/* OFF by default */

	_AIR_on = 0;

	_monitor_period = MT_NORMAL;

	_balancing_target = V_CRITICAL_H;

	_override = 0;

	stack_init();

	set_CPU_priority(7);
	set_mode(OP_INITIALIZING);
	update_loop_period(_monitor_period);


	/*
	 * Configuration phase
	 */

	timer1_config();	/* for time keeping; no need for real time */

	IO_config();

	CAN1_config();
	CAN2_config();


	/* Enable Watchdog Timer; will remain active from here on. *
	 * From here, main loop (and the main loop only!) should   *
	 * reset the timer.                                        */
	RCONbits.SWDTEN = 1;

	timer2_config();	/* for main loop */


	/*
	 * Initialization complete
	 */

	set_mode(OP_NORMAL);
	set_CPU_priority(0);


	while(1){
		Idle();
	}

	return 0;
}
