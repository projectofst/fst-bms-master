/**********************************************************************
 *   FST BMS --- master
 *
 *   Code revision stamp
 *   Configuration parameters
 *   Error codes
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "CAN_IDs/CAN_ID.h"


/*
 * Firmware revision: FIRMWARE_H.FIRMWARE_L
 */

#define FIRMWARE_H 0
#define FIRMWARE_L 5


/*
 * Configuration parameters
 */

#define SLAVES_N			12	/* number of slaves; consider changing to STACKS_N (it's just covering the FST05e which has more slaves than stacks) */

#define DOS_ALLOWED_FAULTS				10	/* number of DoS allowed faults */
#define CRITICAL_VALUES_ALLOWED_FAULTS	3	/* number of critical values allowed faults; if slave doesn't respond after a critical value being issued, this will trigger too */

/*
 * Monitoring levels
 * corresponds to monitoring periods in ms:
 */

#define MT_NORMAL		(1000/SLAVES_N)
#define MT_ALERT		(500/SLAVES_N)
#define MT_CRITICAL		(250/SLAVES_N)


/*
 * Internal definitions
 */

/* CPU core clock and instruction clock values
   ADC should be tuned if this is changed      */
#define FOSC 30000000UL
#define FCY FOSC/4

#endif
