# Readme

## Info

This code is part of *FST BMS*.

## Code structure

This project makes use of submodules!
Remember that when cloning.

Code specific to the master module is within src/


## Contributing

This program is distributed under GNU GPLv2.

Any patches or improvements are welcomed.
The proper way of submitting them should be negotiated by email with current maintainers case by case, including request for direct write access to repository.
